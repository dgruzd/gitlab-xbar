module { "name": "gitlab" };
import "utils" as utils;

# Varibales passed through --arg
def gitlabURL: $gitlabURL;
def username: $username;
def images: $images[0];

def dashboard($type; $relation):
  "\(gitlabURL)/dashboard/\($type)?scope=all&state=opened&\($relation)_username=\(username)";

def image($name):
  images[$name | ascii_downcase]?;

def todo_reason:
  if .reason == "assigned" then "☎️"
  elif .reason == "approval_required" then "📝"
  elif .reason == "build_failed" then "💣"
  elif .reason == "directly_addressed" then "☎️"
  elif .reason == "marked" then "✅"
  elif .reason == "member_access_requested" then "📝"
  elif .reason == "mentioned" then "☎️"
  elif .reason == "merge_train_removed" then "💣"
  elif .reason == "review_requested" then "🕵️"
  elif .reason == "review_submitted" then "🕵️"
  elif .reason == "unmergeable" then "💣"
  else .reason
  end;

def todos: if .data.currentUser.todos != null then
  .data.currentUser.todos as $data | {
    hasNextPage: $data.pageInfo.hasNextPage,
    count: $data.list | length,
    title: "todos",
    attrs: {
      href: "\(gitlabURL)/dashboard/todos",
      templateImage: images.todo
    },
    list: $data.list | map({
      title: "\(. | todo_reason) \(. | utils::createdAt) [\(.project.name // .group.name)] \(.title)",
      attrs: {
        href: .target.url,
        templateImage: image(.target.type)
      }
    })
  }
else
  {
    title: "todos",
    attrs: { templateImage: images.todo },
    list: [
      { title: "Could not access GitLab!" },
      { title: "Are you offline?" },
      { title: "Is your access token outdated?" }
    ]
  }
end | [
  ({ title: .count?, attrs: { image: images.gitlab } } | utils::title),
  "---",
  (. | utils::submenu),
  "---"
] | join("\n");

def mergeRequests($relation): if .data.currentUser.mergeRequests != null then
  .data.currentUser.mergeRequests as $data | {
    hasNextPage: $data.pageInfo.hasNextPage,
    count: $data.list | length,
    title: $relation,
    attrs: {
      href: dashboard("merge_requests"; $relation),
      templateImage: images.mergerequest
    },
    list: $data.list | map({
      title:"[\(.project.name)] \(.title)",
      attrs: {
        href: .url,
        templateImage: (image("pipeline\(.headPipeline?.status)") // images.mergerequest)
      }
    })
  } | [
    (. | utils::submenu),
    "---"
  ] | join("\n")
else
  empty
end;

def issues($relation): if .data.issues != null then
  .data.issues as $data | {
    hasNextPage: $data.pageInfo.hasNextPage,
    count: $data.list | length,
    title: $relation,
    attrs: {
      href: dashboard("issues"; $relation),
      templateImage: images.issue,
    },
    list: $data.list | map({
      title: .title,
      attrs: {
        href: .url,
        templateImage: images.issue
      }
    })
  } | [
    (. | utils::submenu),
    "---"
  ] | join("\n")
else
  empty
end;

def version: if .data.metadata.version != null then
  {
    title: .data.metadata.version,
    attrs: {
      href: "\(gitlabURL)/help",
      templateImage: images.deployment
    }
  } | [
    (. | utils::title),
    "---"
  ] | join("\n")
else
  empty
end;

def refresh: {
  title: "refresh data",
  attrs: {
    templateImage: images.retry,
    refresh: true
  }
} | utils::title;
